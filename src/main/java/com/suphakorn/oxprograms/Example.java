/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.suphakorn.oxprograms;

import java.util.Scanner;

/**
 *
 * @author exhau
 */
class Example {

    static int add(int a, int b) {
        return a+b;
    }

    static String Chup(char player1, char player2) {
        if(player1 ==  's' && player2 == 'p'){
            return "p1";
        }else if(player1 ==  'h' && player2 == 's'){
            return "p1";
        }else if(player1 ==  'p' && player2 == 'h'){
            return "p1";
        }else if(player1 ==  'p' && player2 == 's'){
            return "p2";
        }else if(player1 ==  's' && player2 == 'h'){
            return "p2";
        }else if(player1 ==  'h' && player2 == 'p'){
            return "p2";
        }
        return "draw";
    }
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        
        System.out.println("Please input player 1 (p,h,s): ");
        char player1 = kb.next().charAt(0);
        System.out.println("Please input player 2 (p,h,s): ");
        char player2 = kb.next().charAt(0);
        String winner = Chup(player1, player2);
        System.out.println("Winner is " + winner);
        
    }
    
}
