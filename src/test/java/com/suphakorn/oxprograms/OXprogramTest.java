/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.suphakorn.oxprograms;

import static com.suphakorn.oxprograms.OXProgram.count;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author exhau
 */
public class OXprogramTest {
    
    public OXprogramTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckVCerticalCol1Win() {
        char table[][] = {{'O', '-', '-'},    
                          {'O', '-', '-'}, 
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table,currentPlayer,col));
    }
    
    @Test
    public void testCheckVCerticalCol2Win() {
        char table[][] = {{'-', 'O', '-'},    
                          {'-', 'O', '-'}, 
                          {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table,currentPlayer,col));
    }
    
    @Test
    public void testCheckVCerticalCol3NoWin() {
        char table[][] = {{'-', '-', 'X'},    
                          {'-', '-', 'O'}, 
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(false, OXProgram.checkVertical(table,currentPlayer,col));
    }
    
    @Test
    public void testCheckPlayerOcol2Win() {
        char table[][] = {{'-', 'O', '-'},    
                          {'-', 'O', '-'}, 
                          {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        int row = 2;
        assertEquals(true, OXProgram.checkWin(table,currentPlayer,row,col));
    }
    
    @Test
    public void testCheckPlayerOcheckX1Win() {
        char table[][] = {{'O', '-', '-'},    
                          {'-', 'O', '-'}, 
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX1(table,currentPlayer));
    }
    
    @Test
    public void testCheckPlayerOcheckX2Win() {
        char table[][] = {{'-', '-', 'O'},    
                          {'-', 'O', '-'}, 
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX2(table,currentPlayer));
    }
    
    @Test
    public void testCheckHorizonrow1Win() {
        char table[][] = {{'O', 'O', 'O'},    
                          {'-', '-', '-'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table,currentPlayer,row));
    }
    
    @Test
    public void testCheckHorizonrow2Win() {
        char table[][] = {{'-', '-', '-'},    
                          {'O', 'O', 'O'}, 
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true, OXProgram.checkHorizontal(table,currentPlayer,row));
    }
    
    @Test
    public void testCheckHorizonrow3Win() {
        char table[][] = {{'-', '-', '-'},    
                          {'-', '-', '-'}, 
                          {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true, OXProgram.checkHorizontal(table,currentPlayer,row));
    }
    
    @Test
    public void testCheckDraw() {
        char table[][] = {{'O', 'X', 'O'},    
                          {'O', 'X', 'X'}, 
                          {'X', 'O', 'O'}};
        char currentPlayer = 'O';
        count = 9;
        assertEquals(true, OXProgram.checkDraw(count));
    }
    
    

}
