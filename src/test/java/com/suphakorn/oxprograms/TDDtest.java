/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.suphakorn.oxprograms;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author exhau
 */
public class TDDtest {
    
    public TDDtest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    
    // add (1,2) is 3
    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1,2));
    }
    
    // add (3,4) is 7
    @Test
    public void testAdd_3_4is7() {
        assertEquals(7, Example.add(3,4));
    }
    
    // add (20,22) is 44
    @Test
    public void testAdd_20_22is44() {
        assertEquals(42, Example.add(20,22));
    }
    
    //rock scissor paper
    // char(player 1,player 2)  -> "p1","p2","Draw"
    
    @Test
    public void tesChup_p1_s_p2_s_is_p1() {
        
        assertEquals("p1", Example.Chup('s','p'));
    }
    
    @Test
    public void tesChup_p1_h_p2_s_is_p1() {
        
        assertEquals("p1", Example.Chup('h','s'));
    }
    
    @Test
    public void tesChup_p1_h_p2_p_is_p2() {
        
        assertEquals("p2", Example.Chup('h','p'));
    }
    
    public void tesChup_p1_p_p2_s_is_p2() {
        
        assertEquals("p2", Example.Chup('p','s'));
    }
    
    @Test
    public void tesChup_p1_s_p2_h_is_p2() {
        
        assertEquals("p2", Example.Chup('s','h'));
    }
    
    @Test
    public void tesChup_p1_p_p2_p_is_draw() {
        
        assertEquals("draw", Example.Chup('p','p'));
    }
    
    
    
     
    
}
